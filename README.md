# Car Marketplace #


### Technologies Invloved  ###

* ReactJS - Frontend
* Laravel - Backend


### For Frontend setup ###

* Enter into the  web folder
* npm install   - To install dependencies 
* npm run start - Run the frontend. By default it's run through the port number 3000


### For Backend setup ###

* Enter into the api folder
* composer update - To install dependencies 
* modify the DB settings in .env file
* php artisan migrate - Create/ update DB schema
* php artisan serve   - To run the backend

